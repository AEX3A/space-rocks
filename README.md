# Space Rocks
### Informations
- description : simple space shooter game
- type : personnal project
- date : 2019
---
### Usage
- prerequisites :
  - The language Python (download Python 3.7 [here](https://www.python.org/downloads/))
  - The librairy Pygame (install Pygame 2 with the command ```pip install pygame```)
- execution : 
  - open a terminal in the "space_rocks" folder
  - execute the command ```py space_rocks.py```
- controls :
  - shoot -> SPACE
  - accelerate -> UP / Z
  - rotate left -> LEFT / Q
  - rotate right -> RIGHT / D