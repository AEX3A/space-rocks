import os
import sys
import pygame
from pygame.locals import *
from pygame.math import Vector2
from random import *

import load
import global_variables

class Laser(pygame.sprite.Sprite):
    def __init__(self, position, rotation):
        #Initialisation du sprite
        pygame.sprite.Sprite.__init__(self)
        #self.image_laser = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\Lasers\laserBlue15.png', -1)
        #self.image_explosion = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\Lasers\laserBlue09.png', -1)
        self.image_laser = load.load_image('spaceshooter/PNG/Lasers/laserBlue15.png', -1)
        self.image_explosion = load.load_image('spaceshooter/PNG/Lasers/laserBlue09.png', -1)
        #Initialisation de la position du sprite
        self.rect = self.image_laser.get_rect()
        self.rect.left = position[0]
        self.rect.top = position[1]
        #Ajout de la valeur rect du meteor dans la liste des hitbox
        global_variables.lasers_hitbox.append((self, self.rect))
        #Initialisation des variables
        self.move_speed = 400
        self.rotation = rotation
        self.direction = Vector2(0, -4).rotate(self.rotation)
        self.velocity = Vector2()
        self.is_exploding = False


    def move(self,delta):
        #Déplacement du laser
        self.velocity = self.direction * self.move_speed * delta
        self.rect.move_ip(round(self.velocity.x), round(self.velocity.y))
        #Restriction de la position du laser
        if (self.rect.left < 0 or self.rect.left > global_variables.screen_size.width) or (self.rect.top < 0 or self.rect.top > global_variables.screen_size.height):
            self.is_exploding = True


    def hitbox(self):
        #Mise à jour de la hitbox du laser
        for i in range(len(global_variables.lasers_hitbox)):
            if global_variables.lasers_hitbox[i][0] == self:
                global_variables.lasers_hitbox[i] = (self,self.rect)

        i = 0
        meteor_found = False
        #Gestion d'un tir de laser sur une météorite
        while i < len(global_variables.meteors_hitbox) and not meteor_found:
            if self.rect.colliderect(global_variables.meteors_hitbox[i][1]):
                meteor_found = True
                #Comportement du laser en cas de collision
                self.is_exploding = True
                self.image_laser.set_alpha(0)
                global_variables.liste_explosion.append((global_variables.seconds,self.rect))
                #global_variables.draw_image(self.image_explosion,0,self.rect.left,self.rect.top)
                #Comportement de la météorite en cas de collision
                global_variables.liste_meteors.pop(global_variables.liste_meteors.index(global_variables.meteors_hitbox[i][0]))
                global_variables.meteors_hitbox.pop(i)
            i += 1


    def explode(self):
        global_variables.liste_lasers.pop(global_variables.liste_lasers.index(self))
        y = 0
        indice_laser = None
        while y < len(global_variables.lasers_hitbox) and indice_laser is None:
            if global_variables.lasers_hitbox[y][0] is self:
                indice_laser = y
            y += 1
        if y < len(global_variables.lasers_hitbox):
            global_variables.lasers_hitbox.pop(y)


    def draw(self):
        global_variables.draw_image(self.image_laser,self.rotation,self.rect.left,self.rect.top)


    def update(self, delta):
        #Déplacement du laser
        self.move(delta)
        #Hitbox du laser
        self.hitbox()
        #Affichage du laser
        self.draw()

        #Si le laser sort de l'écran ou entre en collision avec une météorite, il est supprimé du jeu
        if self.is_exploding:
            self.explode()