import os
import sys
import pygame
from pygame.locals import *
from pygame.math import Vector2

import global_variables as self
import load
import player
import meteor
import laser

#Pygame
screen = None
screen_size = None

#Jeu
clock = None
start_time = None
seconds = None
delta = None
inputs = []
level = None
pause = None
game_over = None
quit_game = None
background = None
font_small = None
font_medium = None
font_big = None
selection = None
selection_color = None

#Player
liste_players = []
player_hitbox = []
min_player_hp = None
max_player_hp = None
player_hp = None
regen_player_hp = None

#Meteors
number_meteors = None
liste_meteors = []
meteors_hitbox = []

#Lasers
liste_lasers = []
lasers_hitbox = []
liste_explosion = []
duration_explosion = None

def init_game():
    #Initialisation de pygame
    pygame.init()
    #Initialisation de l'écran de jeu en fullscreen
    self.screen = pygame.display.set_mode()
    self.screen_size = self.screen.get_rect()
    #Initilisation du titre de la fenètre principale
    pygame.display.set_caption('Space Rocks')
    #Variables générales du jeu
    self.clock = pygame.time.Clock()
    self.pause = False
    self.game_over = True
    self.quit_game = False
    #self.background = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\Backgrounds\space_background.jpeg')
    self.background = load.load_image('spaceshooter/Backgrounds/space_background.jpeg')
    self.background = pygame.transform.scale(self.background, (self.screen_size.width, self.screen_size.height))
    self.font_small = pygame.font.Font(None, 30)
    self.font_medium = pygame.font.Font(None, 90)
    self.font_big = pygame.font.Font(None, 150)
    self.selection = 0
    self.selection_color = (0,200,0)
    #Affichage de messages d'erreur en cas de librairies manquantes
    if not pygame.font:
        print('Warning, fonts disabled')
    if not pygame.mixer:
        print('Warning, sound disabled')


def start_game():
    #Initialisation des différentes variables du jeu
    #Jeu
    self.start_time = pygame.time.get_ticks()
    self.game_over = False
    self.pause = False
    self.level = 1
    #Player
    self.min_player_hp = 0
    self.max_player_hp = 100
    self.player_hp = max_player_hp
    self.regen_player_hp = 2
    #Meteors
    self.number_meteors = 5*self.level
    #Lasers
    self.duration_explosion = 0.25
    #Initialisation des objects du niveau
    init_game_objects(True,True)


def start_next_level():
    #Jeu
    self.level += 1
    #Player
    for current_player in self.liste_players:
        current_player.start_immunity = self.seconds
    #Meteors
    self.number_meteors = 5*self.level
    #Initialisation et suppression des objects du niveau
    self.delete_game_objects(False,True,True)
    self.init_game_objects(False,True)


def end_game():
    self.selection = 0
    self.game_over = True
    self.pause = False
    self.delete_game_objects(True,True,True)


#Instanciation des différents objets du jeu
def init_game_objects(p,m):
    if p:
        self.seconds = (pygame.time.get_ticks()-self.start_time)/1000
        self.liste_players.append(player.Player())
    if m:
        for i in range(self.number_meteors):
            self.liste_meteors.append(meteor.Meteor())


#Suppression des instances des objets du jeu
def delete_game_objects(p,m,l):
    if p:
        self.liste_players.clear()
        self.player_hitbox.clear()
    if m:
        self.liste_meteors.clear()
        self.meteors_hitbox.clear()
    if l:
        self.liste_lasers.clear()
        self.lasers_hitbox.clear()    


def get_inputs():
    self.inputs = []
    pygame.event.pump()
    keys = pygame.key.get_pressed()
    if (keys[K_UP]):
        self.inputs.append('PRESSED_UP')
    elif (keys[K_z]):
        self.inputs.append('PRESSED_Z')
    if (keys[K_DOWN]):
        self.inputs.append('PRESSED_DOWN')
    elif (keys[K_s]):
        self.inputs.append('PRESSED_S')
    if (keys[K_LEFT]):
        self.inputs.append('PRESSED_LEFT')
    elif (keys[K_q]):
        self.inputs.append('PRESSED_Q')
    if (keys[K_RIGHT]):
        self.inputs.append('PRESSED_RIGHT')
    elif (keys[K_d]):
        self.inputs.append('PRESSED_D')
    if (keys[K_SPACE]):
        self.inputs.append('PRESSED_SPACE')
    
    for event in pygame.event.get():
        if event.type == QUIT:
            self.inputs.append('QUIT')
        if event.type == KEYUP and event.key == K_ESCAPE:
            self.inputs.append('RELEASED_ESCAPE')
        if event.type == KEYUP and event.key == K_RETURN:
            self.inputs.append('RELEASED_ENTER')
        if event.type == KEYUP and event.key == K_SPACE:
            self.inputs.append('RELEASED_SPACE')
        if event.type == KEYUP and event.key == K_UP:
            self.inputs.append('RELEASED_UP')
        if event.type == KEYUP and event.key == K_z:
            self.inputs.append('RELEASED_Z')
        if event.type == KEYUP and event.key == K_DOWN:
            self.inputs.append('RELEASED_DOWN')
        if event.type == KEYUP and event.key == K_s:
            self.inputs.append('RELEASED_S')
        if event.type == KEYUP and event.key == K_LEFT:
            self.inputs.append('RELEASED_LEFT')
        if event.type == KEYUP and event.key == K_q:
            self.inputs.append('RELEASED_Q')
        if event.type == KEYUP and event.key == K_RIGHT:
            self.inputs.append('RELEASED_RIGHT')
        if event.type == KEYUP and event.key == K_d:
            self.inputs.append('RELEASED_D')


def update_game():
    #Mise à jour du temps écoulé dans le jeu
    self.seconds = (pygame.time.get_ticks()-self.start_time)/1000
    #Mise à jour du background
    screen.blit(self.background, (0, 0))
    #Mise à jour des différentes classes du jeu
    for current_player in self.liste_players:
        current_player.update(delta)
    for current_meteor in self.liste_meteors:
        current_meteor.update(delta)
    for current_laser in self.liste_lasers:
        current_laser.update(delta)
    image_explosion = load.load_image('spaceshooter/PNG/Lasers/laserBlue09.png', -1)
    for current_explosion in self.liste_explosion:
        if current_explosion[0] + self.duration_explosion >= self.seconds:
            transparence = ((current_explosion[0] + self.duration_explosion - self.seconds)/self.duration_explosion)*255
            image_explosion.set_alpha(transparence)
            self.draw_image(image_explosion,0,current_explosion[1].left,current_explosion[1].top)
            image_explosion.set_alpha()
        else:
            self.liste_explosion.pop(self.liste_explosion.index(current_explosion))
    #Mise à jour de la barre de hp
    hp_bar_size = (600,30)
    hp_bar_position = (self.screen_size.width/2-hp_bar_size[0]/2, 20)
    #hp_bar = pygame.Surface(hp_bar_size)
    hp_bar_background = pygame.draw.rect(self.screen, (100, 100, 100),(hp_bar_position,hp_bar_size))
    hp_bar_progress = pygame.draw.rect(self.screen, (0, 200, 0),(hp_bar_position,(player_hp*(hp_bar_size[0]/100),hp_bar_size[1])))
    #Mise à jour du texte de l'interface
    if pygame.font:
        text_seconds = self.font_small.render('Temps écoulé : '+str(int(self.seconds))+'s', 1, (255, 255, 255))
        text_hp = self.font_small.render(str(int(self.player_hp))+' %', 1, (255, 255, 255))
        text_level = self.font_medium.render('LEVEL '+str(int(self.level)), 1, (255, 255, 255))
        self.screen.blit(text_seconds, (25,25))
        self.screen.blit(text_hp, (self.screen_size.width/2-text_hp.get_size()[0]/2,25))
        self.screen.blit(text_level, (25,self.screen_size.height-text_level.get_size()[1]-25))
    #Mise à jour de l'écran
    pygame.display.flip()

    #Pause du jeu si la touche Echap est utilisée
    if 'RELEASED_ESCAPE' in self.inputs:
        self.pause = True 

    #Fin du niveau si il ne reste pas de météorites sur l'écran
    if len(self.liste_meteors) == 0:
        self.start_next_level()

    #Fin du jeu si le joueur n'a plus de hp
    if self.player_hp <= self.min_player_hp:
        self.end_game()


def home_menu():
    #Gestion des actions de l'utilisateur
    i = 0
    key_released = False
    while not key_released and i < len(self.inputs):
        if self.inputs[i] == 'RELEASED_ENTER':
            key_released = True
            if self.selection == 0:
                self.start_game()
            elif self.selection == 1:
                pass
            elif self.selection == 2:
                global_variables.quit_game = True
        if self.inputs[i] == 'RELEASED_UP' or self.inputs[i] == 'RELEASED_Z':
            key_released = True
            if self.selection > 0:
                self.selection -= 1
        if self.inputs[i] == 'RELEASED_DOWN' or self.inputs[i] == 'RELEASED_S':
            key_released = True
            if self.selection < 2:
                self.selection += 1
        i+=1
    #Affichage du menu
    self.screen.blit(self.background, (0, 0))
    if pygame.font:
        text_space_rocks = self.font_big.render('Space Rocks', 1, (255, 255, 255))
        if self.selection == 0:
            text_play = self.font_medium.render('PLAY', 1, self.selection_color)
        else: 
            text_play = self.font_medium.render('PLAY', 1, (255, 255, 255))
        if self.selection == 1:
            text_options = self.font_medium.render('OPTIONS', 1, self.selection_color)
        else: 
            text_options = self.font_medium.render('OPTIONS', 1, (255, 255, 255))
        if self.selection == 2:
            text_quit = self.font_medium.render('QUIT', 1, self.selection_color)
        else: 
            text_quit = self.font_medium.render('QUIT', 1, (255, 255, 255))
        title_pos = (self.screen_size.width/2,self.screen_size.height/10)
        menu_pos = (self.screen_size.width/2,self.screen_size.height/2)
        ecart = 20
        self.screen.blit(text_space_rocks, (title_pos[0]-text_space_rocks.get_size()[0]/2,title_pos[1]-text_space_rocks.get_size()[1]/2))
        self.screen.blit(text_play, (menu_pos[0]-text_play.get_size()[0]/2,menu_pos[1]-text_play.get_size()[1]/2))
        self.screen.blit(text_options, (menu_pos[0]-text_options.get_size()[0]/2,menu_pos[1]-text_options.get_size()[1]/2+text_play.get_size()[1]+ecart))
        self.screen.blit(text_quit, (menu_pos[0]-text_quit.get_size()[0]/2,menu_pos[1]-text_quit.get_size()[1]/2+text_play.get_size()[1]*2+ecart*2))
    pygame.display.flip()


def pause_menu():
    #Gestion des actions de l'utilisateur
    i = 0
    key_released = False
    while not key_released and i < len(self.inputs):
        if self.inputs[i] == 'RELEASED_ENTER':
            key_released = True
            if self.selection == 0:
                self.pause = not self.pause
            elif self.selection == 1:
                pass
            elif self.selection == 2:
                self.end_game()
        if self.inputs[i] == 'RELEASED_UP' or self.inputs[i] == 'RELEASED_Z':
            key_released = True
            if self.selection > 0:
                self.selection -= 1
        if self.inputs[i] == 'RELEASED_DOWN' or self.inputs[i] == 'RELEASED_S':
            key_released = True
            if self.selection < 2:
                self.selection += 1
        i+=1
    #Affichage du menu
    self.screen.blit(self.background, (0, 0))
    if pygame.font:
        text_space_rocks = self.font_big.render('Space Rocks', 1, (255, 255, 255))
        if self.selection == 0:
            text_resume = self.font_medium.render('RESUME', 1, self.selection_color)
        else: 
            text_resume = self.font_medium.render('RESUME', 1, (255, 255, 255))
        if self.selection == 1:
            text_options = self.font_medium.render('OPTIONS', 1, self.selection_color)
        else: 
            text_options = self.font_medium.render('OPTIONS', 1, (255, 255, 255))
        if self.selection == 2:
            text_surrend = self.font_medium.render('SURREND', 1, self.selection_color)
        else: 
            text_surrend = self.font_medium.render('SURREND', 1, (255, 255, 255))
        title_pos = (self.screen_size.width/2,self.screen_size.height/10)
        menu_pos = (self.screen_size.width/2,self.screen_size.height/2)
        ecart = 20
        self.screen.blit(text_space_rocks, (title_pos[0]-text_space_rocks.get_size()[0]/2,title_pos[1]-text_space_rocks.get_size()[1]/2))
        self.screen.blit(text_resume, (menu_pos[0]-text_resume.get_size()[0]/2,menu_pos[1]-text_resume.get_size()[1]/2))
        self.screen.blit(text_options, (menu_pos[0]-text_options.get_size()[0]/2,menu_pos[1]-text_options.get_size()[1]/2+text_resume.get_size()[1]+ecart))
        self.screen.blit(text_options, (menu_pos[0]-text_options.get_size()[0]/2,menu_pos[1]-text_options.get_size()[1]/2+text_resume.get_size()[1]+ecart))
        self.screen.blit(text_surrend, (menu_pos[0]-text_surrend.get_size()[0]/2,menu_pos[1]-text_surrend.get_size()[1]/2+text_resume.get_size()[1]*2+ecart*2))
    pygame.display.flip()


def draw_image(image, rotation_image, x, y):
    # calculate the axis aligned bounding box of the rotated image
    w, h = image.get_size()
    box = [Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(rotation_image) for p in box]
    min_box = (min(box_rotate, key=lambda p: p[0])[
                0], min(box_rotate, key=lambda p: p[1])[1])
    max_box = (max(box_rotate, key=lambda p: p[0])[
                0], max(box_rotate, key=lambda p: p[1])[1])

    # calculate the translation of the pivot
    pivot = pygame.math.Vector2(w/2, -h/2)
    pivot_rotate = pivot.rotate(rotation_image)
    pivot_move = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    origin = (x - w/2 +
                min_box[0] - pivot_move[0], y - h/2 - max_box[1] + pivot_move[1])
    rotated_image = pygame.transform.rotate(image, -rotation_image)
    screen.blit(rotated_image, origin)
    #pygame.draw.rect(screen, (255, 0, 0),(*origin, *rotated_image.get_size()), 2)
