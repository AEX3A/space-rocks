import os
import sys
import pygame
from pygame.locals import *
from pygame.math import Vector2
from random import *

import load
import global_variables

class Meteor(pygame.sprite.Sprite):
    def __init__(self):
        #Initialisation du sprite
        pygame.sprite.Sprite.__init__(self)
        sizes = ['big','med','small','tiny']
        colors = ['Brown','Grey']
        #Choix aléatoire entre une taille big ou med
        #Les tailles small et tiny ne peuvent pas être utilisées par une météorite car elles sont trop petites
        self.size = sizes[randint(0,len(sizes)-1)]
        self.color = colors[randint(0,len(colors)-1)]
        if self.size == 'big':
            my_number = str(randint(1,4))
        else:
            my_number = '1'
        #self.image = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\Meteors\meteorBrown_big1.png', -1)
        self.image = load.load_image('spaceshooter/PNG/Meteors/meteor'+self.color+'_'+self.size+my_number+'.png', -1)
        self.image = pygame.transform.scale2x(self.image)
        #Initialisation de la position du sprite
        self.rect = self.image.get_rect()
        self.rect.left = randint(0,global_variables.screen_size.width)
        self.rect.top = randint(0,global_variables.screen_size.height)
        #Ajout de la valeur rect du meteor dans la liste des hitbox
        global_variables.meteors_hitbox.append((self, self.rect))
        #Initialisation des variables
        self.min_move_speed = randint(25,75)
        self.max_move_speed = 200
        self.move_speed = self.min_move_speed
        self.friction = 200
        self.rotation = 0
        self.min_rotation_speed = 2*self.move_speed
        self.max_rotation_speed = 800
        self.rotation_speed = self.min_move_speed
        self.direction = Vector2(0, -4).rotate(randint(0,360))
        self.velocity = Vector2()
        self.is_colliding = False


    def move(self,delta):
        #Rotation de la météorite
        if self.rotation_speed > self.min_rotation_speed:
            self.rotation_speed -= self.friction * delta
        self.rotation += self.rotation_speed * delta
        self.rotation = self.rotation % 360
        #Déplacement de la météroite
        if self.move_speed > self.min_move_speed:
            self.move_speed -= self.friction * delta
        self.velocity = self.direction * self.move_speed * delta
        self.rect.move_ip(round(self.velocity.x), round(self.velocity.y))

        #Restriction de la vitesse de déplacement et de rotation
        if self.move_speed < self.min_move_speed:
            self.move_speed = self.min_move_speed
        if self.move_speed > self.max_move_speed:
            self.move_speed = self.max_move_speed
        if self.rotation_speed < self.min_rotation_speed:
            self.rotation_speed = self.min_rotation_speed
        if self.rotation_speed > self.max_rotation_speed:
            self.rotation_speed = self.max_rotation_speed

        #Restriction de la position de la météorite
        if self.rect.top < -self.rect.height/2:
            self.rect.top = global_variables.screen_size.height + self.rect.height/2
        if self.rect.left < -self.rect.width/2:
            self.rect.left = global_variables.screen_size.width + self.rect.height/2
        if self.rect.top > global_variables.screen_size.height + self.rect.height/2:
            self.rect.top = -self.rect.height/2
        if self.rect.left > global_variables.screen_size.width + self.rect.width/2:
            self.rect.left = -self.rect.width/2


    def hitbox(self):
        #Mise à jour de la hitbox de la météorite
        for i in range(len(global_variables.meteors_hitbox)):
            if global_variables.meteors_hitbox[i][0] == self:
                global_variables.meteors_hitbox[i] = (self,self.rect)
            #Gestion de la collision avec une autre météorite
            elif self.rect.colliderect(global_variables.meteors_hitbox[i][1]) and not self.is_colliding:
                #Caractéristiques de la météorite qui est entrée en collision avec la météorite actuelle
                meteorite = global_variables.meteors_hitbox[i][0]
                direction = meteorite.direction
                speed = meteorite.move_speed
                sizes = ['tiny','small','med','big']
                size = sizes.index(meteorite.size)
                meteorite.is_colliding = True
                self.is_colliding = True
                meteorite.collision(self.direction,self.move_speed,sizes.index(self.size)/2)
                self.collision(direction,speed,size/2)
                meteorite.is_colliding = False
                self.is_colliding = False


    def collision(self,direction_collision,speed_collision,force_collision=1):
        self.direction = direction_collision
        self.move_speed = self.move_speed + speed_collision * force_collision
        self.rotation_speed = self.move_speed


    def draw(self):
        global_variables.draw_image(self.image,self.rotation,self.rect.left,self.rect.top)


    def update(self, delta):
        #Déplacement et rotation de la météorite
        self.move(delta)
        #Gestion de la hitbox de la météorite
        self.hitbox()
        #Affichage de la météorite
        self.draw()