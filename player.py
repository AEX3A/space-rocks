import os
import sys
import pygame
from pygame.locals import *
from pygame.math import Vector2

import load
import global_variables
import laser

class Player(pygame.sprite.Sprite):
    def __init__(self):
        #Initialisation du sprite du vaisseau
        pygame.sprite.Sprite.__init__(self)
        """ 
        self.image_spaceship = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\playerShip1_red.png', -1)
        self.image_boost_min = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\Effects/fire04.png', -1)
        self.image_boost_medium = load.load_image('D:\Etudes\ENSIBS\Projet\Mini jeu\space-rocks\sprites\spaceshooter\PNG\Effects/fire06.png', -1)
        """
        self.image_spaceship = load.load_image('spaceshooter/PNG/playerShip1_red.png', -1)
        self.image_boost_min = load.load_image('spaceshooter/PNG/Effects/fire04.png', -1)
        self.image_boost_medium = load.load_image('spaceshooter/PNG/Effects/fire06.png', -1)
        #Initialisation de la position du vaisseau
        self.rect = self.image_spaceship.get_rect()
        self.rect.left = global_variables.screen_size.width/2
        self.rect.top = global_variables.screen_size.height/1.5
        #Initialisation des variables du vaisseau
        self.min_move_speed = 50
        self.max_move_speed = 200
        self.move_speed_active = self.min_move_speed
        self.move_speed_passive = self.move_speed_active
        self.acceleration = 200
        self.friction = 150
        self.rotation = 0
        self.rotation_speed = 200
        self.direction_active = Vector2(0, -4).rotate(self.rotation)
        self.direction_passive = self.direction_active
        self.velocity = Vector2()
        #Le joueur est immunisé au lancement du jeu
        self.start_immunity = global_variables.seconds
        self.duration_immunity = 0.25
        self.start_shooting_delay = None
        self.shooting_delay = 0.05


    def move(self,delta):
        #Gestion du déplacement et de la rotation du vaisseau en fonction des touches pressées
        is_up = False
        for input in global_variables.inputs:
            self.direction_passive = Vector2(0, -4).rotate(self.rotation)
            self.rotation = self.rotation % 360
            if input == 'PRESSED_UP' or input == 'PRESSED_Z':
                is_up = True
                self.direction_active = self.direction_passive       
            elif input == 'PRESSED_LEFT' or input == 'PRESSED_Q':
                self.rotation -= self.rotation_speed * delta
            elif input == 'PRESSED_RIGHT' or input == 'PRESSED_D':
                self.rotation += self.rotation_speed * delta
        #La vitesse du vaisseau augmente jusqu'à max_move_speed lorsque la touche UP ou Z est pressée
        if is_up and self.move_speed_active < self.max_move_speed:
            self.move_speed_active += self.acceleration * delta
            self.move_speed_passive = self.move_speed_active
        #La vitesse du vaisseau est affectée par la friction si les touches UP et Z ne sont pas pressées
        elif not is_up and self.move_speed_passive > self.min_move_speed:
            self.move_speed_active = self.min_move_speed
            self.move_speed_passive -= self.friction * delta
        #Déplacement du vaisseau en fonction du vecteur velocity
        self.velocity = self.direction_active * self.move_speed_passive * delta
        self.rect.move_ip(round(self.velocity.x), round(self.velocity.y))

        #Restriction de la vitesse de déplacement et de rotation
        if self.move_speed_active < self.min_move_speed:
            self.move_speed_active = self.min_move_speed
        if self.move_speed_passive < self.min_move_speed:
            self.move_speed_passive = self.min_move_speed
        if self.move_speed_active > self.max_move_speed:
            self.move_speed_active = self.max_move_speed
        if self.move_speed_passive > self.max_move_speed:
            self.move_speed_passive = self.max_move_speed

        #Restriction de la position du vaisseau
        if self.rect.top < -self.rect.height/2:
            self.rect.top = global_variables.screen_size.height + self.rect.height/2
        if self.rect.left < -self.rect.width/2:
            self.rect.left = global_variables.screen_size.width + self.rect.height/2
        if self.rect.top > global_variables.screen_size.height + self.rect.height/2:
            self.rect.top = -self.rect.height/2
        if self.rect.left > global_variables.screen_size.width + self.rect.width/2:
            self.rect.left = -self.rect.width/2


    def shoot(self,delta):
        position_laser = Vector2(self.rect.left, self.rect.top)
        position_laser += self.direction_passive.normalize()*55
        if ('PRESSED_SPACE' in global_variables.inputs) and self.start_shooting_delay == None:
            global_variables.liste_lasers.append(laser.Laser(position_laser,self.rotation))
            self.start_shooting_delay = global_variables.seconds

        
        if self.start_shooting_delay is not None:
            if self.start_shooting_delay + self.shooting_delay < global_variables.seconds:
                self.start_shooting_delay = None


    def hitbox(self,delta):
        #Détection de la colision entre le vaisseau et une météorite
        for hitbox in global_variables.meteors_hitbox:
            if self.rect.colliderect(hitbox[1]):
                if self.start_immunity is None:
                    #Caractéristiques de la météorite qui est entrée en collision avec la météorite actuelle
                    meteorite = hitbox[0]
                    direction = meteorite.direction
                    speed = meteorite.move_speed
                    sizes = ['tiny','small','med','big']
                    size = sizes.index(meteorite.size)
                    #Comportement du vaisseau en cas de collision
                    global_variables.player_hp -= 5*(size+1)
                self.start_immunity = global_variables.seconds
        #Affichage de l'immunité du vaisseau avec un effet de transparence
        if self.start_immunity is not None:
            if self.start_immunity + self.duration_immunity >= global_variables.seconds:
                self.image_spaceship.set_alpha(50)
                self.image_boost_medium.set_alpha(50)
                self.image_boost_min.set_alpha(50)
            else:
                self.image_spaceship.set_alpha()
                self.image_boost_medium.set_alpha()
                self.image_boost_min.set_alpha()
                self.start_immunity = None

        #Mise à jour de la hitbox du vaisseau
        global_variables.player_hitbox = [self,self.rect]

        #Regénération des hp du vaisseau
        if global_variables.player_hp < global_variables.max_player_hp:
            global_variables.player_hp += global_variables.regen_player_hp * delta
        #Restriction des hp du vaisseau
        if global_variables.player_hp < global_variables.min_player_hp:
            global_variables.player_hp = global_variables.min_player_hp
        if global_variables.player_hp > global_variables.max_player_hp:
            global_variables.player_hp = global_variables.max_player_hp


    def draw(self):
        # affichage du boost correspondant à la vitesse du vaisseau
        pos = Vector2(self.rect.left, self.rect.top)
        pos -= self.direction_passive.normalize()*55
        pos1 = Vector2(self.rect.left, self.rect.top)
        pos1 -= self.direction_passive.rotate(-30).normalize()*55
        pos2 = Vector2(self.rect.left, self.rect.top)
        pos2 -= self.direction_passive.rotate(30).normalize()*55

        if self.move_speed_active <= self.min_move_speed:
            pass
        elif self.move_speed_active > self.min_move_speed and self.move_speed_active < self.max_move_speed:
            global_variables.draw_image(self.image_boost_min, self.rotation+180, pos.x, pos.y)
        else:
            global_variables.draw_image(self.image_boost_min, self.rotation+180, pos.x, pos.y)
            global_variables.draw_image(self.image_boost_medium,self.rotation+180,  pos1.x, pos1.y)
            global_variables.draw_image(self.image_boost_medium,self.rotation+180,  pos2.x, pos2.y)

        # affichage du vaisseau
        global_variables.draw_image(self.image_spaceship, self.rotation, self.rect.left, self.rect.top)


    def update(self, delta):
        #Déplacement et rotation du vaisseau
        self.move(delta)
        #Tir de laser
        self.shoot(delta)
        #Hitbox et hp du vaisseau
        self.hitbox(delta)
        #Affichage du vaisseau
        self.draw()