import os
import sys
import pygame
from pygame.locals import *
from pygame.math import Vector2

import load
import global_variables
import player
import meteor

#Initialisation des variables globales du jeu
global_variables.init_game()
#Lancement de la boucle du jeu
while not global_variables.quit_game:
    #Gestion des actions de l'utilisateur
    global_variables.get_inputs()
    #Arret du jeu si l'utilisateur ferme le programme python
    for input in global_variables.inputs:
        if input == 'QUIT':
            global_variables.quit_game = True
    #Affichage du jeu
    if not global_variables.game_over:
        global_variables.delta = (global_variables.clock.tick(80))/1000
        if not global_variables.pause:
            global_variables.update_game()
        else:
            global_variables.pause_menu()
    else:
        global_variables.home_menu()
#Fin du jeu
pygame.quit()
quit()